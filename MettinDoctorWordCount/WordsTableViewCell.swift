//
//  WordsTableViewCell.swift
//  MettinDoctorWordCount
//
//  Created by manuel on 05/07/2020.
//  Copyright © 2020 manuel. All rights reserved.
//

import UIKit

class WordsTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
