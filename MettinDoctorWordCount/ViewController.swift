//
//  ViewController.swift
//  MettinDoctorWordCount
//
//  Created by manuel on 04/07/2020.
//  Copyright © 2020 manuel. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ViewController: UIViewController {
    @IBOutlet weak var stackRepo: UIStackView!
    @IBOutlet weak var titleLbl: UILabel!
    var fileName = ""
    var listFileStr = [String]()
    //la idea es aprovechar la api de git para listar los archivo por pantalla
    private func downloadJson() {
        let request = AF.request("https://gitlab.com/api/v4/projects/19765982/repository/tree")
        request.responseJSON {response in
            let responseJSON = JSON(response.data!)
            for index in 0...responseJSON.count-1{
                self.listFileStr.append(responseJSON[index]["name"].description)
            }
            self.addButton()
        }
    }

    func addButton(){
        for index in 0...listFileStr.count-1{
            let button = UIButton()
            button.addTarget(self, action: #selector(sayAction(_:)), for: .touchUpInside)
            stackRepo.addArrangedSubview(button)
            button.setTitle(listFileStr[index], for: .normal)
            button.backgroundColor = .white
            button.layer.cornerRadius = 5
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor.blue.cgColor
            button.setTitleColor(UIColor.blue, for: .normal)
        }
    }
    @objc private func sayAction(_ sender: UIButton?) {
        self.fileName = sender?.titleLabel?.text ?? ""
        performSegue(withIdentifier: "DownloadFile", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? UINavigationController,
            let targetController = vc.topViewController as? TextViewController {
            targetController.fileName = fileName
        }        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadJson()
    }
}

