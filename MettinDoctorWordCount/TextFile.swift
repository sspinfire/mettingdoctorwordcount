//
//  TextFile.swift
//  MettinDoctorWordCount
//
//  Created by manuel on 05/07/2020.
//  Copyright © 2020 manuel. All rights reserved.
//

import Foundation
struct structDicWord {
    var str = ""
    var value = 0
}
class TextFile{
    var fileName = ""
    var counterLetter = ""
    var rawText = ""
    var wordDic: [String: Int] = [:]
    func setRawText(_ raw: String){
        rawText = raw
    }
    func setFileName(_ name: String){
        fileName = name
    }
    func setCounterLetter(_ counter: String){
        counterLetter = counter
    }
    func finderCase(_ searchText: String)->[structDicWord] {
        let dictionary = wordDic.filter({$0.key.lowercased().prefix(searchText.count) == searchText.lowercased()})
        var strcWords = [structDicWord]()
        var word = structDicWord()
        for (key,value) in dictionary{
            word.str = key
            word.value = value
            strcWords.append(word)
        }
        return strcWords
    }
    //NO acaba de funcionar bien
    func orderOrigin()->[structDicWord]{
        var strcWords = [structDicWord]()
        var word = structDicWord()
        for (key,value) in wordDic{
            word.str = key
            word.value = value
            strcWords.append(word)
        }
        return strcWords
    }
    func orderAlpha()->[structDicWord]{
        let dictionary = wordDic.sorted{ $0.key < $1.key }
        var strcWords = [structDicWord]()
        var word = structDicWord()
        for (key,value) in dictionary{
            word.str = key
            word.value = value
            strcWords.append(word)
        }
        return strcWords
    }
    func orderApar()->[structDicWord]{
        let dictionary = wordDic.sorted{ $0.value > $1.value }
        var strcWords = [structDicWord]()
        var word = structDicWord()
        for (key,value) in dictionary{
            word.str = key
            word.value = value
            strcWords.append(word)
        }
        return strcWords
    }
    func counterWord(){
        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.decimalDigits).union(.uppercaseLetters).union(.punctuationCharacters)
        let components = rawText.components(separatedBy: chararacterSet)
        let words = components.filter { !$0.isEmpty }
        counterLetter = String(words.count)
    }
    func listWords()->[structDicWord]{
        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.decimalDigits).union(.punctuationCharacters).union(.symbols)
        let components = rawText.components(separatedBy: chararacterSet)
        let strList = components.filter { !$0.isEmpty }.map{$0.capitalized}
        let dictionary = strList.reduce(into: [:]) { counts, letter in
            counts[letter, default: 0] += 1
        }
        wordDic = dictionary
        return orderAlpha()
    }
}
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
