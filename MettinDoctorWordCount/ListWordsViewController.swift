//
//  ListWordsViewController.swift
//  MettinDoctorWordCount
//
//  Created by manuel on 05/07/2020.
//  Copyright © 2020 manuel. All rights reserved.
//

import UIKit
class ListWordsViewController: UIViewController{
    @IBOutlet weak var wordSearchBar: UISearchBar!
    @IBOutlet weak var wordTableView: UITableView!
    @IBAction func backButtonOnClick(_ sender: Any) {
                self.dismiss(animated: true, completion: nil)
    }
    var textFile = TextFile()
    var listStr = [structDicWord]()
    var searchList = [structDicWord]()
    var searching = false
    override func viewDidLoad() {
        super.viewDidLoad()
        wordSearchBar.delegate = self
        listStr = textFile.listWords()
    }
}
extension ListWordsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching{
         return searchList.count
        }
        else{
            return listStr.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WordsTableViewCell
        if searching{
            cell.title.text = searchList[indexPath.row].str
            cell.subTitle.text = String(searchList[indexPath.row].value)
        }
        else{
            cell.title.text = listStr[indexPath.row].str
            cell.subTitle.text = String(listStr[indexPath.row].value)
        }
        return cell
    }
}

extension ListWordsViewController: UISearchBarDelegate{
    func filterSearchBar(_ index: Int,_ searchText: String){
        switch index {
        case 0:
            if searchText == ""{
                listStr = textFile.orderAlpha()
                searching = false
                wordTableView.reloadData()
            }
            else{
                searching = true
                searchList = textFile.finderCase(searchText)
                wordTableView.reloadData()
            }
        case 1:
            if searchText == ""{
                listStr = textFile.orderApar()
                searching = false
                wordTableView.reloadData()
            }
            else{
                searchList = textFile.finderCase(searchText)
                searching = true
                wordTableView.reloadData()
            }
        case 2:
            if searchText == ""{
                listStr = textFile.orderOrigin()
                searching = false
                wordTableView.reloadData()
            }
            else{
                searching = true
                wordTableView.reloadData()
                searchList = textFile.finderCase(searchText)
            }
        default:
            print("no type")
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterSearchBar(searchBar.selectedScopeButtonIndex,searchText)
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
            filterSearchBar(selectedScope,searchBar.text ?? "")
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        wordTableView.reloadData()
    }
}
