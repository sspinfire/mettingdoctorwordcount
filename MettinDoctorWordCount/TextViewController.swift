//
//  TextViewController.swift
//  MettinDoctorWordCount
//
//  Created by manuel on 05/07/2020.
//  Copyright © 2020 manuel. All rights reserved.
//

import UIKit
import Alamofire

class TextViewController: UIViewController {
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var fileText: UITextView!
    @IBOutlet weak var counterLetter: UILabel!
    @IBOutlet weak var listWordsButton: UIButton!
    var textFile: TextFile = TextFile()
    var fileName = ""
    @IBAction func listWordsOnClick(_ sender: Any) {
        performSegue(withIdentifier: "ListWords", sender: self)
    }
    @IBAction func backButtonOnClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? UINavigationController,
            let targetController = vc.topViewController as? ListWordsViewController {
            targetController.textFile = textFile
        }
    }
    private func downloadText() {
        AF.request("https://gitlab.com/sspinfire/archivestest/-/raw/master/" + fileName + "?inline=false").downloadProgress(closure: { (progress) in
            self.progressBar.progress = Float(progress.fractionCompleted)
        }).responseData { (response) in
            if let data = response.value {self.fileText.text = String(data: data, encoding: .utf8)
                self.textFile.setRawText(self.fileText.text)
                self.counterWord()
            }
        }
    }
    func counterWord(){
        let chararacterSet = CharacterSet.whitespacesAndNewlines.union(.decimalDigits)
        let components = fileText.text.components(separatedBy: chararacterSet)
        let words = components.filter { !$0.isEmpty }
        counterLetter.text = String(words.count)
        textFile.setCounterLetter(counterLetter.text!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadText()
    }
}
